# aws_dynamodb_table

resource "aws_dynamodb_table" "Temperature" {
    
    name = "Temperature"
    read_capacity  = 20
  write_capacity = 20

  hash_key       = "id"

  attribute {
    name = "id"
    type = "S"
  }
}